/*
 * ConnectionManager.h
 *
 *  Created on: Aug 20, 2015
 *      Author: Anthony
 */

#ifndef CONNECTIONMANAGER_H_
#define CONNECTIONMANAGER_H_

#define SSID_LEN_MAX 32
#define BSSID_LEN_MAX 6

int16_t ConnectToNetwork(SlWlanMode_t desiredMode);
void ConnectonManagerInit(void);
int16_t ConnectionManagerGetMode(void);
uint32_t GetDefaultGatewayAddress(void);

#endif /* CONNECTIONMANAGER_H_ */
