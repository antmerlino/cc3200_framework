/*
 * system.h
 *
 *  Created on: Aug 23, 2015
 *      Author: Anthony
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

#define ERR_PRINT(x) UART_Printf(UART0, "Error [%d] at line [%d] in function [%s]  \n\r",x,__LINE__,__FUNCTION__)

// check the error code and handle it
#define ASSERT_ON_ERROR(error_code)\
{\
	 if(error_code < 0) \
	   {\
			ERR_PRINT(error_code);\
			return error_code;\
	 }\
}

/*****************************
 * LOG UART CONFIGURATION
 ****************************/
#define USE_UART0
#define SUBSYS_UART UART0
#define UART0_TX_BUFFER_LENGTH 256
#define UART0_TX_GPIO 1
#define UART0_RX_GPIO 2

/*****************************
 * LIBRARY INCLUDES
 ****************************/
#include "library.h"
#include "buffer.h"
#include "buffer_printf.h"
#include "uart.h"
#include "timing.h"
#include "list.h"
#include "task.h"
#include "subsys.h"

#endif /* SYSTEM_H_ */
