# README #

This is a basic project structure for the CC3200. It depends on the "Embedded Library" published by Anthony Merlino and Michael Muhlbaier. 

The "Embedded Library" provides a middle ground between running the Simplelink library with a RTOS like FreeRTOS or running without an operating system.
