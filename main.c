/*
 * main.c
 */

// Standard includes
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

// Driverlib includes
#include "hw_types.h"
#include "hw_ints.h"
#include "hw_memmap.h"
#include "driverlib/interrupt.h"
#include "driverlib/prcm.h"
#include "driverlib/gpio.h"
#include "driverlib/pin.h"
#include "driverlib/utils.h"

// Simplelink includes
#include "simplelink.h"
#include "netcfg.h"

// Embedded Library includes
#include "system.h"

#include "ConnectionManager.h"

/* Vector Table defined in startup_ccs.c */
extern void (* const g_pfnVectors[])(void);


int main(void) {
	// Set the interrupt vector table
	IntVTableBaseSet((unsigned long)&g_pfnVectors[0]);

	//Disable master interrupt during configuration
	IntMasterDisable();

	Timing_Init();
	Task_Init();
	Task_SetIdleTask(_SlNonOsMainLoopTask);
	UART_Init(UART0);
	
    //
    // Configure PIN_58 (GPIO3) for GPIOInput
    //
	PRCMPeripheralClkEnable(PRCM_GPIOA0, PRCM_RUN_MODE_CLK);
    PinTypeGPIO(PIN_58, PIN_MODE_0, false);
    GPIODirModeSet(GPIOA0_BASE, 0x8, GPIO_DIR_MODE_IN);

	ConnectonManagerInit();

	IntMasterEnable();
	IntEnable(FAULT_SYSTICK);

    // Check SW0 for start-up mode
    if(GPIO_READ(3)){
    	ConnectToNetwork(ROLE_AP);
    } else {
    	ConnectToNetwork(ROLE_STA);
    }

	while(1){
		SystemTick();
	}
}

void SimpleLinkHttpServerCallback(SlHttpServerEvent_t *pSlHttpServerEvent, SlHttpServerResponse_t *pSlHttpServerResponse){
    switch (pSlHttpServerEvent->Event){
    	case SL_NETAPP_HTTPGETTOKENVALUE_EVENT:
			break;
    	case SL_NETAPP_HTTPPOSTTOKENVALUE_EVENT:
    		break;
    }
}

void SimpleLinkSockEventHandler(SlSockEvent_t *pSock){


}


