/*
 * ConnectionManager.c
 *
 *  Created on: Aug 20, 2015
 *      Author: Anthony
 */

// Standard Includes
#include <stdint.h>
#include <stdlib.h>

// Simplelink includes
#include "simplelink.h"
#include "netcfg.h"

// Driverlib includes
#include "hw_types.h"
#include "hw_ints.h"
#include "hw_memmap.h"
#include "driverlib/interrupt.h"
#include "driverlib/prcm.h"
#include "driverlib/gpio.h"
#include "driverlib/pin.h"
#include "driverlib/utils.h"

// Project Includes
#include "ConnectionManager.h"
#include "system.h"

/*
 * Macros
 */
#define SL_STOP_TIMEOUT 200
#define AUTO_CONNECTION_TIMEOUT_COUNT 50 /* 5 Sec */
#define WLAN_DEL_ALL_PROFILES 0xFF

#define LED_GPIO 9

#define LED_On() GPIOPinWrite(GET_GPIO_PORT(LED_GPIO), GET_GPIO_BIT(LED_GPIO), GET_GPIO_BIT(LED_GPIO))
#define LED_Off() GPIOPinWrite(GET_GPIO_PORT(LED_GPIO), GET_GPIO_BIT(LED_GPIO), 0)

/*
 * Variables
 */
uint8_t connectionSSID[SSID_LEN_MAX+1];
uint8_t connectionBSSID[BSSID_LEN_MAX];

uint32_t defaultGatewayAddress;

struct connection{
	int8_t internetState;
	union {
		uint16_t state;
		struct{
			uint8_t nwpInitialized : 1;
			uint8_t connected : 1;
			uint8_t ipLeased : 1;
			uint8_t ipAqcuired : 1;
			uint8_t smartConfigStarted : 1;
			uint8_t p2pDeviceFound : 1;
			uint8_t connectionFailed : 1;
			uint8_t pingComplete : 1;
		};
	};
} connection;

uint16_t pingPacketsReceived = 0;
int16_t mode;
uint8_t cm_sysid;

/*
 * Function Declarations
 */
int16_t ConfigureMode(SlWlanMode_t desiredMode);
void ConnectionTest(void);
void SimpleLinkPingReport(SlPingReport_t *pPingReport);
int16_t SmartConfigConnect(void);
int16_t SmartConfigStop(void);


/*
 * Function Definitions
 */
void ConnectonManagerInit(void){
	connection.state = 0;
	connection.internetState = -1;
	memset(connectionSSID, 0, sizeof(connectionSSID));
	memset(connectionBSSID, 0, sizeof(connectionBSSID));

	cm_sysid = Subsystem_Init("Connection Manager", (version_t)0x0000001U, 0);


    //
    // Configure PIN_64 for GPIOOutput
    //
	PRCMPeripheralClkEnable(PRCM_GPIOA1, PRCM_RUN_MODE_CLK);
    PinTypeGPIO(PIN_64, PIN_MODE_0, false);
    GPIODirModeSet(GPIOA1_BASE, 0x2, GPIO_DIR_MODE_OUT);
}

int16_t ConnectionManagerGetMode(void){
	return mode;
}

int16_t ConnectToNetwork(SlWlanMode_t desiredMode){
	int16_t retVal = -1;
	uint8_t timeoutCount = 0;

	retVal = sl_Start(NULL, NULL, NULL);
	ASSERT_ON_ERROR( retVal);

	mode = retVal;

	if(mode != desiredMode){
		if(desiredMode == ROLE_STA){
			// If the current mode is AP mode
			if(mode == ROLE_AP){
				// Need to wait for IP_ACQUIRED before
				while(!connection.ipAqcuired){
					SystemTick();
				}
			}
		}

		// Switch to Desired Mode
		retVal = ConfigureMode(desiredMode);
		ASSERT_ON_ERROR(retVal);
		mode = retVal;
	}

	// If the mode switch was successful
	if(mode == ROLE_STA){
		//Stop Internal HTTP Server
		retVal = sl_NetAppStop(SL_NET_APP_HTTP_SERVER_ID);
		ASSERT_ON_ERROR(retVal);

		//Start Internal HTTP Server
		retVal = sl_NetAppStart(SL_NET_APP_HTTP_SERVER_ID);
		ASSERT_ON_ERROR(retVal);

		//TODO: Write Timing Module to use for this timeout

		// Wait for the device to Auto Connect
		while(timeoutCount < AUTO_CONNECTION_TIMEOUT_COUNT) {
			// If we've acquired an IP or connected
			if(connection.ipAqcuired || connection.connected){
				break;
			}
			//Turn RED LED On
			LED_On();
			WaitMs(50);
			//Turn RED LED Off
			LED_Off();
			WaitMs(50);

			timeoutCount++;
		}

		//Couldn't connect Using Auto Profile
		if(timeoutCount == AUTO_CONNECTION_TIMEOUT_COUNT)
		{
			//Blink Red LED to Indicate Connection Error
			LED_On();

			// Reset Connection State
			connection.state = 0;

			//Connect Using Smart Config
			retVal = SmartConfigConnect();
			ASSERT_ON_ERROR(retVal);

			//Waiting for the device to Auto Connect
			while(!connection.ipAqcuired || !connection.connected){
				WaitMs(500);
			}
		}
		//Turn RED LED Off
		LED_Off();

		ConnectionTest();
		return 0;
	}

	if(mode == ROLE_AP){
		//waiting for the AP to acquire IP address from Internal DHCP Server
		// If the device is in AP mode, we need to wait for this event
		// before doing anything
		while(!connection.ipAqcuired){
			SystemTick();
		}

        //Stop Internal HTTP Server
		retVal = sl_NetAppStop(SL_NET_APP_HTTP_SERVER_ID);
        ASSERT_ON_ERROR(retVal);

        //Start Internal HTTP Server
        retVal = sl_NetAppStart(SL_NET_APP_HTTP_SERVER_ID);
        ASSERT_ON_ERROR(retVal);

        uint8_t i = 0;
        //Blink LED 3 times to Indicate AP Mode
        for(i = 0; i < 3; i++)
        {
            //Turn RED LED On
        	LED_On();
            WaitMs(400);
            //Turn RED LED Off
            LED_Off();
			WaitMs(400);
        }

 	   uint16_t len = SSID_LEN_MAX;
 	   uint16_t config_opt = WLAN_AP_OPT_SSID;
 	   sl_WlanGet(SL_WLAN_CFG_AP_ID, &config_opt , &len, (uint8_t* )connectionSSID);
 	   LogMsg(cm_sysid, "Connect to : \'%s\'", connectionSSID);
 	   return 0;
	}

	LogMsg(cm_sysid, "Simple Link in bad mode : \'%d\'", mode);
	return -1;
}

int16_t ConfigureMode(SlWlanMode_t desiredMode){
	int16_t retVal;

	retVal = sl_WlanSetMode(desiredMode);
	ASSERT_ON_ERROR(retVal);

	// Restart Network Processor
	//TODO: How do we handle the error here?
	sl_Stop(SL_STOP_TIMEOUT);

	// Reset Connection State
	connection.state = 0;

	return sl_Start(NULL, NULL, NULL);
}

/**
 * \brief pings to ip address of domain "www.ti.com"
 *
 * This function pings to the default gateway to ensure the wlan cannection,
 * then check for the internet connection, if present then get the ip address
 * of Domain name "www.ti.com" and pings to it
 */
void ConnectionTest(void){
    int16_t retVal = 0;

    SlPingStartCommand_t PingParams;
    SlPingReport_t PingReport;

    uint32_t IP_Addr;

    // Set the ping parameters
    PingParams.PingIntervalTime = 1000;
    PingParams.PingSize = 10;
    PingParams.PingRequestTimeout = 3000;
    PingParams.TotalNumberOfAttempts = 3;
    PingParams.Flags = 0;

    pingPacketsReceived = 0;
    connection.pingComplete = 0;

    /* Check for Internet connection */
    /* Querying for ti.com IP address */
    retVal = sl_NetAppDnsGetHostByName((signed char *)"www.ti.com", 10, &IP_Addr, SL_AF_INET);
    if (retVal < 0)
    {
        // LAN connection is successful
        // Problem with Internet connection
        connection.internetState = -2;
        return;
    }

    // Replace the ping address to match ti.com IP address
    PingParams.Ip = IP_Addr;

    // Try to ping www.ti.com
    sl_NetAppPingStart((SlPingStartCommand_t*)&PingParams, SL_AF_INET, (SlPingReport_t*)&PingReport, SimpleLinkPingReport);

    while (!connection.pingComplete){
    	SystemTick();
    }

    if (pingPacketsReceived)
    {
        // LAN connection is successful
        // Internet connection is successful
    	pingPacketsReceived = 0;
        connection.internetState = 0;
        return;
    }
    else
    {
        // LAN connection is successful
        // Problem with Internet connection
        connection.internetState = -2;
        return;
    }
}

/**
 * \brief call back function for the ping test
 *
 * @param pPingReport is the pointer to the structure containing the result
 * 			for the ping test
 *
 * \return None
 */
void SimpleLinkPingReport(SlPingReport_t *pPingReport){
    connection.pingComplete = 1;
    pingPacketsReceived = pPingReport->PacketsReceived;
}


//TODO: Finish Handling rest of events
void SimpleLinkWlanEventHandler(SlWlanEvent_t *pWlanEvent){

	slWlanConnectAsyncResponse_t*  pEventData = NULL;

    if(pWlanEvent == NULL){
        LogMsg(cm_sysid, "Null pointer");
        while(1);
    }

	switch(pWlanEvent->Event)
	{
		case SL_WLAN_CONNECT_EVENT:
			connection.connected = 1;

            // Copy new connection SSID and BSSID to global parameters
            memcpy(connectionSSID,pWlanEvent->EventData.STAandP2PModeWlanConnected.ssid_name,
                   pWlanEvent->EventData.STAandP2PModeWlanConnected.ssid_len);
            memcpy(connectionBSSID, pWlanEvent->EventData.STAandP2PModeWlanConnected.bssid,
                   SL_BSSID_LENGTH);

            LogMsg(cm_sysid, "[WLAN EVENT] Device Connected to the AP: %s\r\n"
                       " o BSSID: %x:%x:%x:%x:%x:%x",
					   connectionSSID,connectionBSSID[0],
					   connectionBSSID[1],connectionBSSID[2],
					   connectionBSSID[3],connectionBSSID[4],
					   connectionBSSID[5]);
			break;
		case SL_WLAN_DISCONNECT_EVENT:
			connection.connected = 0;
			connection.ipAqcuired = 0;

            // Information about the disconnected event such as the reason
            // for the disconnection can be used if required
            //

			pEventData = &pWlanEvent->EventData.STAandP2PModeDisconnected;

            // If the user has initiated 'Disconnect' request,
            //'reason_code' is SL_USER_INITIATED_DISCONNECTION
            if(SL_USER_INITIATED_DISCONNECTION == pEventData->reason_code)
            {
                LogMsg(cm_sysid, "[WLAN EVENT] Device disconnected from the AP: %s\r\n"
                           " o BSSID: %x:%x:%x:%x:%x:%x on application's request",
						   connectionSSID,connectionBSSID[0],
						   connectionBSSID[1],connectionBSSID[2],
						   connectionBSSID[3],connectionBSSID[4],
						   connectionBSSID[5]);
            }
            else
            {
            	LogMsg(cm_sysid, "[WLAN ERROR] Device disconnected from the AP AP: %s, "
                           "BSSID: %x:%x:%x:%x:%x:%x on an ERROR..!!",
						   connectionSSID,connectionBSSID[0],
						   connectionBSSID[1],connectionBSSID[2],
						   connectionBSSID[3],connectionBSSID[4],
						   connectionBSSID[5]);
            }

			// Reset the SSID and BSSID
			memset(connectionSSID,0,sizeof(connectionSSID));
			memset(connectionBSSID,0,sizeof(connectionBSSID));
			break;
        case SL_WLAN_STA_CONNECTED_EVENT:
            // when device is in AP mode and any client connects to device cc3xxx
            //SET_STATUS_BIT(g_ulStatus, STATUS_BIT_CONNECTION);
            //CLR_STATUS_BIT(g_ulStatus, STATUS_BIT_CONNECTION_FAILED);

            //
            // Information about the connected client (like SSID, MAC etc) will
            // be available in 'slPeerInfoAsyncResponse_t' - Applications
            // can use it if required
            //
            // slPeerInfoAsyncResponse_t *pEventData = NULL;
            // pEventData = &pSlWlanEvent->EventData.APModeStaConnected;
            //

            LogMsg(cm_sysid, "[WLAN EVENT] Station connected to device");
            break;
        case SL_WLAN_STA_DISCONNECTED_EVENT:
            // when client disconnects from device (AP)
            //CLR_STATUS_BIT(g_ulStatus, STATUS_BIT_CONNECTION);
            //CLR_STATUS_BIT(g_ulStatus, STATUS_BIT_IP_LEASED);

            //
            // Information about the connected client (like SSID, MAC etc) will
            // be available in 'slPeerInfoAsyncResponse_t' - Applications
            // can use it if required
            //
            // slPeerInfoAsyncResponse_t *pEventData = NULL;
            // pEventData = &pSlWlanEvent->EventData.APModestaDisconnected;
            //

            LogMsg(cm_sysid, "[WLAN EVENT] Station disconnected from device");
        	break;
        case SL_WLAN_SMART_CONFIG_COMPLETE_EVENT:
            //SET_STATUS_BIT(g_ulStatus, STATUS_BIT_SMARTCONFIG_START);

            //
            // Information about the SmartConfig details (like Status, SSID,
            // Token etc) will be available in 'slSmartConfigStartAsyncResponse_t'
            // - Applications can use it if required
            //
            //  slSmartConfigStartAsyncResponse_t *pEventData = NULL;
            //  pEventData = &pSlWlanEvent->EventData.smartConfigStartResponse;
            //
        	break;
        case SL_WLAN_SMART_CONFIG_STOP_EVENT:
            // SmartConfig operation finished
            //CLR_STATUS_BIT(g_ulStatus, STATUS_BIT_SMARTCONFIG_START);

            //
            // Information about the SmartConfig details (like Status, padding
            // etc) will be available in 'slSmartConfigStopAsyncResponse_t' -
            // Applications can use it if required
            //
            // slSmartConfigStopAsyncResponse_t *pEventData = NULL;
            // pEventData = &pSlWlanEvent->EventData.smartConfigStopResponse;
            //
        	break;
        default:
             LogMsg(cm_sysid, "[WLAN EVENT] Unexpected event [0x%x]", pWlanEvent->Event);
        	break;
	}
}

uint32_t GetDefaultGatewayAddress(void){
	return defaultGatewayAddress;
}


void SimpleLinkNetAppEventHandler(SlNetAppEvent_t *pNetAppEvent){
	SlIpV4AcquiredAsync_t *pEventData = NULL;

    if(pNetAppEvent == NULL)
    {
        LogMsg(cm_sysid, "Null pointer");
        while(1);
    }

    switch(pNetAppEvent->Event)
    {
        case SL_NETAPP_IPV4_IPACQUIRED_EVENT:
        	connection.ipAqcuired = 1;

            //
            // Information about the IP Aqcuired event such as the
        	// IP, DNS, and gateway can be used if desired
            //
        	pEventData = &pNetAppEvent->EventData.ipAcquiredV4;

        	defaultGatewayAddress = pEventData->gateway;

            LogMsg(cm_sysid, "[NETAPP EVENT] IP Acquired:\r\n o IP=%d.%d.%d.%d\r\n o Gateway=%d.%d.%d.%d",
					SL_IPV4_BYTE(pNetAppEvent->EventData.ipAcquiredV4.ip,3),
					SL_IPV4_BYTE(pNetAppEvent->EventData.ipAcquiredV4.ip,2),
					SL_IPV4_BYTE(pNetAppEvent->EventData.ipAcquiredV4.ip,1),
					SL_IPV4_BYTE(pNetAppEvent->EventData.ipAcquiredV4.ip,0),
					SL_IPV4_BYTE(pNetAppEvent->EventData.ipAcquiredV4.gateway,3),
					SL_IPV4_BYTE(pNetAppEvent->EventData.ipAcquiredV4.gateway,2),
					SL_IPV4_BYTE(pNetAppEvent->EventData.ipAcquiredV4.gateway,1),
					SL_IPV4_BYTE(pNetAppEvent->EventData.ipAcquiredV4.gateway,0));

        	break;
        case SL_NETAPP_IP_LEASED_EVENT:
            connection.ipLeased = 1;

            //
            // Information about the IP-Leased details(like IP-Leased,lease-time,
            // mac etc) will be available in 'SlIpLeasedAsync_t' - Applications
            // can use it if required
            //
            // SlIpLeasedAsync_t *pEventData = NULL;
            // pEventData = &pNetAppEvent->EventData.ipLeased;
            //
            break;
        case SL_NETAPP_IP_RELEASED_EVENT:
        	connection.ipLeased = 0;
            //
            // Information about the IP-Released details (like IP-address, mac
            // etc) will be available in 'SlIpReleasedAsync_t' - Applications
            // can use it if required
            //
            // SlIpReleasedAsync_t *pEventData = NULL;
            // pEventData = &pNetAppEvent->EventData.ipReleased;
            //
        	break;
        default:
        	LogMsg(cm_sysid, "[NETAPP EVENT] Unexpected event [0x%x] ", pNetAppEvent->Event);
        	break;
    }
}

//*****************************************************************************
//
//!   \brief Connecting to a WLAN Accesspoint using SmartConfig provisioning
//!
//!    This function enables SmartConfig provisioning for adding a new
//!    connection profile to CC3200. Since we have set the connection policy
//!    to Auto, once SmartConfig is complete,CC3200 will connect
//!    automatically to the new connection profile added by smartConfig.
//!
//!   \param[in]               None
//!
//! \return  0 - Success
//!            -1 - Failure
//!
//!   \warning           If the WLAN connection fails or we don't acquire an
//!                         IP address,We will be stuck in this function forever.
//!
//*****************************************************************************
int16_t SmartConfigConnect(void){
    uint8_t policyVal;
    int16_t retVal = -1;

    //TODO: Decide if we want to delete profiles. I think probably not

    // Clear all profiles
    // This is of course not a must, it is used in this example to make sure
    // we will connect to the new profile added by SmartConfig
//    retVal = sl_WlanProfileDel(WLAN_DEL_ALL_PROFILES);
//    ASSERT_ON_ERROR(retVal);

    LogMsg(cm_sysid, "Starting SmartConfig. Use SmartConfig to connect");

    // Set AUTO policy
    retVal = sl_WlanPolicySet(SL_POLICY_CONNECTION,
                                SL_CONNECTION_POLICY(1,0,0,0,0),
                                &policyVal,
                                1 /*PolicyValLen*/);
    ASSERT_ON_ERROR(retVal);

    // Start SmartConfig
    // This example uses the unsecured SmartConfig method
    retVal = sl_WlanSmartConfigStart(0,                  //groupIdBitmask
                           SMART_CONFIG_CIPHER_NONE,      //cipher
                           0,                             //publicKeyLen
                           0,                             //group1KeyLen
                           0,                             //group2KeyLen
                           NULL,                          //publicKey
                           NULL,                          //group1Key
                           NULL);                         //group2Key
    ASSERT_ON_ERROR(retVal);

    return 0;
}


//*****************************************************************************
//
//!
//!    \brief Stop SmartConfig provisioning
//!
//!    This function Stops SmartConfig provisioning
//!
//!    \param[in]                   None
//!
//! \return  0 - Success
//!            -1 - Failure
//!
//!   \note
//!
//*****************************************************************************
int16_t SmartConfigStop(void){
	int16_t retVal = -1;

	retVal = sl_WlanSmartConfigStop();
    ASSERT_ON_ERROR(retVal);

    return 0;
}










